package trz.uoregon.edu;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Implementation of Logistic Regression algorithm for CIS 572. 
 * 
 * @author  Troy Zuroske, University of Oregon
 */

public class logistic 
{
	static float eta;
	static float sigma;           
	static String trainingFile;
	static String testData;
	static String modelFile;
	static float accuracy; 
	static String labels[]; 
	static int MAX_ITER = 100;
	static double epsilon = .00001; 
	static int attributes; 
	static int iter = 0; 
	
	/**
	 * Main class that initializes algorithm and reads in files placing data 
	 * into array lists.
	 * 
	 * @param args - 1. training file
	 * 				 2. test file
	 * 				 3. eta
	 * 				 4. sigma
	 * 				 5. output file
	 * 
	 * @return nothing
	 */

	public static void main(String[] args) 
	{
		ArrayList<ArrayList<Float>> trainingDataSet = new ArrayList<ArrayList<Float>>(); 
		ArrayList<ArrayList<Float>> testDataSet = new ArrayList<ArrayList<Float>>(); 
		ArrayList <Float> finalWeights = new ArrayList <Float>();
		double accuracy; 
		String sLine; 
		String line[];
		String label;
		String finalLabels[] = null; 
		
		if (args.length == 5)
    	{	
			trainingFile = args[0]; 
			testData = args[1];
			eta = (Float.parseFloat(args[2]));
			sigma = (Float.parseFloat(args[3]));
			modelFile = args[4];
    	}
    	else
    	{
    		System.err.println("Argument error: must be in format "
    				+ "<training-set> <test-set> <eta> <sigma> <model-file>");
    		System.exit(0); 
    	}
		
		try {
				BufferedReader newFileBuffer = new BufferedReader(new FileReader(trainingFile));
				label = newFileBuffer.readLine(); 
				labels = label.split(","); 
				attributes = (labels.length - 1); 
				finalLabels = Arrays.copyOf(labels, (labels.length - 1)); 
				while ((sLine  = newFileBuffer.readLine()) != null) 
				{		
					line = sLine.split(",");
					ArrayList<Float> convert = new ArrayList<Float>();
					for (int i = 0; i < line.length; i++)
					{
					    try 
					    {
					        convert.add(Float.parseFloat(line[i]));
					    } catch (NumberFormatException nfe) {};
					}
					
					trainingDataSet.add(convert); 
				}
				
				newFileBuffer.close();
				
				newFileBuffer = new BufferedReader(new FileReader(testData));
				newFileBuffer.readLine(); 
				while ((sLine  = newFileBuffer.readLine()) != null) 
				{		
					line = sLine.split(",");
					ArrayList<Float> convertTest = new ArrayList<Float>();
					for (int i = 0; i < line.length; i++)
					{
					    try 
					    {
					    	convertTest.add(Float.parseFloat(line[i]));
					    } catch (NumberFormatException nfe) {};
					}
					
					testDataSet.add(convertTest); 
				}
				
				newFileBuffer.close();
			} catch (Exception e) 
			{
				e.printStackTrace();
			}
		
		finalWeights = optimizeParams(trainingDataSet, eta, sigma);
		try 
		{
			PrintWriter writer = new PrintWriter(new FileWriter(modelFile));
			writer.println("Bias: " + finalWeights.get((finalWeights.size()-1)));
			for (int i = 0; i < (finalLabels.length); i++)
			{
				writer.printf("%-20s %20.16f\n", finalLabels[i], finalWeights.get(i)); 
			}
			

			writer.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		
		accuracy = checkAccuracy(finalWeights, testDataSet); 
		System.out.println("Accuracy: " + accuracy + "%");
	}
	
	public static ArrayList<Float> optimizeParams (ArrayList<ArrayList<Float>> trainingDataSet, 
			float eta, float sigma)
	{
		ArrayList<Float> weights = new ArrayList<Float>(); 
		ArrayList<Float> priorWeights = new ArrayList<Float>(); 
		float sigmaConst = 1/((sigma)*(sigma)); 
		float gradient = 0; 
		
		weights.addAll(trainingDataSet.get(0));   
		weights.remove(weights.size() - 1); 
		for (int i = 0; i < weights.size(); i++) 
		{
			weights.set(i, (float) 0); 
			priorWeights.add(i, (float) 0);
		}
		weights.add((float) 0);
		priorWeights.add((float) 0);
		gradient = (float) (epsilon + 1); 
		
		while ((gradient > epsilon) && (iter < MAX_ITER))
		{
			for (int i = 0; i < weights.size(); i++) 
			{
				priorWeights.set(i, weights.get(i));
			} 
            iter+=1; 
            
            for (int i = 0; i < weights.size(); i++)
            {
            	if (i == (weights.size() - 1))
            	{
            		weights.set(i, priorWeights.get(i) + eta * 
            				probSum(priorWeights, trainingDataSet, i)); 
            	}
            	else
            	{
            		weights.set(i, (sigmaConst*priorWeights.get(i)) + eta * 
            				probSum(priorWeights, trainingDataSet, i));
            	}
            }
            
            gradient = computeGradient(priorWeights, weights);
		}
		return weights; 
	}
	
	public static float computeGradient (ArrayList<Float> oldWeights,
											ArrayList<Float> weights)
	{ 
		float sm = 0;
		float changeWeight = 0; 
		for (int i = 0; i < oldWeights.size() - 1; i++)
		{
			if (i == 0)
			{
				continue;
			}
			changeWeight = weights.get(i)-oldWeights.get(i);
			sm += changeWeight * changeWeight;
		}
		return (float) Math.sqrt(sm); 
	}
	public static float probSum (ArrayList<Float> weights, 
			ArrayList<ArrayList<Float>> trainingDataSet, int i)
	{
		float num = 0;
		float count; 
		for (int k = 0; k < trainingDataSet.size(); k++)
		{
			if (i == (weights.size() - 1))
			{
				count = 1;
			}
			else
			{
				count = trainingDataSet.get(k).get(i);	
			}
			count = count * ((trainingDataSet.get(k).get(trainingDataSet.get(k).size() - 1) 
					- (checkP(trainingDataSet.get(k), weights) ? 1 : 0)));
			num+=count;
		}
		return num; 
	}
	
	public static boolean checkP (ArrayList<Float> trainingDataSet,
									ArrayList<Float> weights)
	{
		boolean bResult; 
		if (1 <= pOfY(trainingDataSet, weights))
		{
			bResult = true; 
		}
		else
		{
			bResult = false;
		}
		
		return bResult; 
	}
	
	public static double pOfY (ArrayList<Float> trainingDataSet, ArrayList<Float> weights)
	{
		float sm = 0;
		float expon = 0;
		for (int i = 0; i < trainingDataSet.size() - 1; i++)
		{
			sm += (weights.get(i) * trainingDataSet.get(i)); 
		}
		expon = weights.get(weights.size() - 1) + sm; 
		if (expon > 700)
		{
			expon = 700; 
		}
		
		return Math.exp(expon); 
	}
	
	public static double checkAccuracy (ArrayList<Float> finalWeights,
			ArrayList<ArrayList<Float>> testDataSet)
	{
		double correct = 0;
		double total = 0;
		
		for (int i = 0; i < testDataSet.size(); i++)
		{
			Boolean bFinal = new Boolean(false); 
			Integer l = new Integer(0);
			int iB = 0; 
			bFinal = prediction(testDataSet.get(i), finalWeights); 
			iB = bFinal ? 1 : 0; 
			l = Math.round(testDataSet.get(i).get(testDataSet.get(i).size()-1)); 
			
			if (iB == l)
			{
				correct+=1; 
			}
			total+=1;
		}
		return ((correct)/(total)) * 100; 
		
	}
	
	public static boolean prediction (ArrayList<Float> testDataSet, 
									ArrayList<Float> weights)
	{
		double prob = pOfY(testDataSet, weights); 
		System.out.println(prob);
		
		if (prob >= 0.5)
		{
			return true; 
		}
		else
		{
			return false; 
		}
	}
}
