package trz.uoregon.edu.id3;

/**
 * Class that calculates Chi-Squared Test. 
 * 
 * Used : http://en.wikipedia.org/wiki/Pearson%27s_chi-squared_test for 
 * understanding
 * 
 * @author  Troy Zuroske, University of Oregon
 */

public class ChiSquaredCalculation 
{
	static double pVal = 6.635;
	public static ChiCalculation pVal001 = new ChiCalculation(pVal);
	public static class ChiCalculation 
	{
		double criticalValue;
		
		ChiCalculation(double criticalValue)
		{
			this.criticalValue = criticalValue;
		}

		public boolean testPreviousData(int[][] splitOnVal) 
		{
			int vals = 2; 
			int k = 0;
			boolean bReturnChiResult; 
			double splitOnCalc = splitOnVal[0][0] + splitOnVal[1][0] + 
					splitOnVal[0][1] + splitOnVal[1][1];
			double chiTestVal = 0;

			for (int i = 0; i < vals; i++) 
			{
				double aData = (splitOnVal[k][0] + splitOnVal[k][1]) / splitOnCalc
						* (splitOnVal[0][i] + splitOnVal[1][i]);
				double bData = splitOnVal[k][i];
				chiTestVal += ((bData - aData) * (bData - aData) / aData);
			}
			bReturnChiResult = chiTestVal > criticalValue + 1e-8;
			
			return bReturnChiResult; 
		}

		public boolean currentTest(int[][] observedVals) 
		{
			double k = 0;
			boolean bResult; 
			double chiTestVal = 0;
			int xLength = observedVals.length;
			int yLength = observedVals[0].length;
			double yMarg[] = new double[yLength];
			double xMarg[] = new double[xLength];
			
			for (int i = 0; i < xLength; i++) 
			{
				for (int x = 0; x < yLength; x++) 
				{
					xMarg[i] += observedVals[i][x];
					k += observedVals[i][x];
				}
			}
			for (int x = 0; x < yLength; x++)
			{
				for (int i = 0; i < xLength; i++) {
					yMarg[x] += observedVals[i][x];
				}
			}
			for (int i = 0; i < xLength; i++) 
			{
				for (int x = 0; x < yLength; x++) 
				{
					double xyMargCalc = 1.0 * xMarg[i] * yMarg[x] / k;
					double observedCalc = observedVals[i][x];
					if (xyMargCalc > 0) {
						chiTestVal += (xyMargCalc - observedCalc) * 
								(xyMargCalc - observedCalc)  / xyMargCalc;
					}
				}
			}
			bResult = chiTestVal > criticalValue + 1e-8;
			
			return bResult; 
		}
	}
}
