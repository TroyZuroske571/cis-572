package trz.uoregon.edu.id3;

import java.util.ArrayList;

import trz.uoregon.edu.id3.ChiSquaredCalculation.ChiCalculation;

/**
 * Class calculates the information gain for the given attributes and then grows
 * the tree recursively based on results. 
 * 
 * Used pseudo code: https://www.cs.swarthmore.edu/~meeden/cs63/f05/id3.html
 * 
 * @author  Troy Zuroske, University of Oregon
 */

public class InformationGainCalculation 
{
	static void calculateGain(treeNode currentNode, 
			EntropyCalculation entropyCalc, ChiCalculation chiCalc, 
			int treeDepth) 
	{	
		int binary = 2; 
		int totalLines[][] = new int[dataLine.uniqueVals][binary];
		int negativeTotal = 0;
		int positiveTotal = 0;
		double gainTopBound = 0;
		int decision = -1;
		double gainVal; 
		
		for (int i = 0; i < currentNode.allDataFromLine.size(); i++)
		{
			if (currentNode.allDataFromLine.get(i).positiveOrNegative == 1)
			{
				positiveTotal++;
			} else {
				negativeTotal++;
			}
		}
		
		for (int z = 0; z < currentNode.totalAtts; z++) 
		{
			int data[][] = new int[dataLine.uniqueVals][2];
			for (dataLine line : currentNode.allDataFromLine)
			{
				if (line.positiveOrNegative == 1)
				{
					data[(int) line.arryVals.values().toArray()[z]][1]++;
				}
				else
				{
					data[(int) line.arryVals.values().toArray()[z]][0]++;
				}
			}
			
			//entropy calculation 
			gainVal = entropyCalc.getEntropy(positiveTotal, negativeTotal);
			
			for (int j = 0; j < dataLine.uniqueVals; j++) 
			{
				gainVal -= (data[j][0] + data[j][1]) * 1.0 
						/ (positiveTotal + negativeTotal)
						* entropyCalc.getEntropy(data[j][0], data[j][1]);
			}

			if (gainVal > gainTopBound)
			{
				gainTopBound = gainVal;
				decision = z;
				for (int i = 0; i < dataLine.uniqueVals; i++)
				{
					totalLines[i][0] = data[i][0];
					totalLines[i][1] = data[i][1];
				}
			}

		}

		if (chiCalc.currentTest(totalLines) && gainTopBound > 1e-10 )
		{
			ArrayList<ArrayList<dataLine>> trainingSetLst =
					new ArrayList<ArrayList<dataLine>>();
			currentNode.nodeTestVal = decision;
			
			for (int w = 0; w < dataLine.uniqueVals; w++) 
			{
				trainingSetLst.add(new ArrayList<dataLine>());
			}

			for (dataLine set : currentNode.allDataFromLine)
				trainingSetLst.get((int) set.arryVals.values().toArray()[decision]).add(set);

			for (int k = 0; k < dataLine.uniqueVals; k++) 
			{	
				if (trainingSetLst.get(k).size() > 0) //recurse test
				{
					currentNode.childrenNodeArry[k] = 
							new treeNode(currentNode, trainingSetLst.get(k));
					calculateGain(currentNode.childrenNodeArry[k], entropyCalc, 
							chiCalc, treeDepth + 1);
				}
			}
		}
	}


}
