package trz.uoregon.edu;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;


/**
 * Implementation of Naive Bayes algorithm for CIS 572. 
 * 
 * @author  Troy Zuroske, University of Oregon
 */

public class nb 
{
	static float beta;          
	static String trainingFile;
	static String testData;
	static String modelFile;
	static float accuracy; 
	static String labels[]; 
	static int MAX_ITER = 100;
	static double epsilon = 0.00001; 
	static int attributes; 
	static int iter = 0; 
	static double bias = 0.0; 
	
	/**
	 * Main class that initializes algorithm and reads in files placing data 
	 * into array lists.
	 * 
	 * @param args - 1. training file
	 * 				 2. test file
	 * 				 3. beta prior
	 * 				 4. output file
	 * 
	 * @return nothing
	 */

	public static void main(String[] args) 
	{
		ArrayList<ArrayList<Float>> trainingDataSet = new ArrayList<ArrayList<Float>>(); 
		ArrayList<ArrayList<Float>> testDataSet = new ArrayList<ArrayList<Float>>(); 
		ArrayList <Float> finalWeights = new ArrayList <Float>();
		double accuracy; 
		String sLine; 
		String line[];
		String label;
		String finalLabels[] = null; 
		
		if (args.length == 4)
    	{	
			trainingFile = args[0]; 
			testData = args[1];
			beta = (Float.parseFloat(args[2]));
			modelFile = args[3];
    	}
    	else
    	{
    		System.err.println("Argument error: must be in format "
    				+ "<training-set> <test-set> <beta> <model-file>");
    		System.exit(0); 
    	}
		
		try {
				BufferedReader newFileBuffer = new BufferedReader(new FileReader(trainingFile));
				label = newFileBuffer.readLine(); 
				labels = label.split(","); 
				attributes = (labels.length - 1); 
				finalLabels = Arrays.copyOf(labels, (labels.length - 1)); 
				while ((sLine  = newFileBuffer.readLine()) != null) 
				{		
					line = sLine.split(",");
					ArrayList<Float> convert = new ArrayList<Float>();
					for (int i = 0; i < line.length; i++)
					{
					    try 
					    {
					        convert.add(Float.parseFloat(line[i]));
					    } catch (NumberFormatException nfe) {};
					}
					
					trainingDataSet.add(convert); 
				}
				
				newFileBuffer.close();
				
				newFileBuffer = new BufferedReader(new FileReader(testData));
				newFileBuffer.readLine(); 
				while ((sLine  = newFileBuffer.readLine()) != null) 
				{		
					line = sLine.split(",");
					ArrayList<Float> convertTest = new ArrayList<Float>();
					for (int i = 0; i < line.length; i++)
					{
					    try 
					    {
					    	convertTest.add(Float.parseFloat(line[i]));
					    } catch (NumberFormatException nfe) {};
					}
					
					testDataSet.add(convertTest); 
				}
				
				newFileBuffer.close();
			} catch (Exception e) 
			{
				e.printStackTrace();
			}
		
		finalWeights = runNB(trainingDataSet, beta);
		try 
		{
			PrintWriter writer = new PrintWriter(new FileWriter(modelFile));
			writer.println("Bias: " + bias);
			for (int i = 0; i < (finalLabels.length); i++)
			{
				writer.printf("%-20s %20.16f\n", finalLabels[i], finalWeights.get(i)); 
			}
			

			writer.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		
		accuracy = checkAccuracy(finalWeights, testDataSet); 
		System.out.println("Accuracy: " + accuracy + "%");
	}
	
	public static ArrayList<Float> runNB (ArrayList<ArrayList<Float>> trainingDataSet, 
			float beta)
	{
		ArrayList<Float> weights = new ArrayList<Float>(); 
		double x = 0.0;
		double y = 0.0;
		double xX = 0.0;
		double yX = 0.0;
		double b1X = 0.0;
		double b1Y = 0.0; 
		
		weights.addAll(trainingDataSet.get(0));   
		weights.remove(weights.size() - 1); 
		for (int i = 0; i < weights.size(); i++) 
		{
			weights.set(i, (float) 0); 
		}
		
		for (int i = 0; i < weights.size(); i++)
		{
			x = computeProb1(trainingDataSet, i, 1, 1, beta);
			y = computeProb1(trainingDataSet, i, 1, 0, beta);
			xX = computeProb1(trainingDataSet, i, 0, 1, beta);
			yX = computeProb1(trainingDataSet, i, 0, 0, beta);
			weights.set(i, (float) (Math.log(x/y) - Math.log(xX/yX))); 
		}
		
		bias = Math.log(probbias(trainingDataSet, 1, beta) / 
				probbias(trainingDataSet, 0, beta));
		
		for (int i = 0; i < weights.size(); i++)
		{
			b1X = computeProb1(trainingDataSet, i, 0, 1, beta);
			b1Y = computeProb1(trainingDataSet, i, 0, 0, beta);

			bias += (Math.log(b1X / b1Y)); 
		}
	
		return weights;
	}
	
	public static float probbias (ArrayList<ArrayList<Float>> trainingDataSet,
			int y, float beta)
	{
		float prob = 0;
		
		int Yi = 0;
		int Yt = 0;
		
		for (int i = 0; i < trainingDataSet.size(); i++)
		{
			if (trainingDataSet.get(i).get(trainingDataSet.get(i).size() - 1) == y)
			{
				Yi+=1;
			}
			Yt+=1; 
		}
		prob = (Yi + beta - 1) / (Yt + (2 * beta) - 2);
		return prob; 
	}
	
	public static float computeProb1 (ArrayList<ArrayList<Float>> trainingDataSet, 
			int loopIter, int x, int y, float beta)
	{
		int XisY = 0;
		int totY = 0; 
		float prob = 0;
		
		for (int k = 0; k < trainingDataSet.size(); k++)
		{
			if (trainingDataSet.get(k).get(trainingDataSet.get(k).size() - 1) == y)
			{
				totY+=1; 
				if (trainingDataSet.get(k).get(loopIter) == x)
				{
					XisY+=1; 
				}
			}
		}
		prob = (XisY + beta - 1)/(totY + (2 * beta) - 1); 
		
		return prob; 
	}
	
	public static double checkAccuracy (ArrayList<Float> finalWeights,
			ArrayList<ArrayList<Float>> testDataSet)
	{
		double correct = 0;
		double total = 0;
		int bPrediction;
		
		for (int i = 0; i < testDataSet.size(); i++)
		{
			bPrediction = prediction(testDataSet.get(i), finalWeights) ? 1 : 0; 
			if (bPrediction == testDataSet.get(i).get(testDataSet.get(i).size() - 1))
			{
				correct+=1;
			}
			total+=1; 
		}
		return (correct/total) * 100; 
	}
	
	public static boolean prediction (ArrayList<Float> testDataSet, 
									  ArrayList<Float> weights)
	{
		double sumOfProb = bias;
		double totalProb = 0.0;
		
		for (int i = 0; i < weights.size(); i++)
		{
			if (testDataSet.get(i) == 1)
			{
				sumOfProb += weights.get(i);
			}
		}
		totalProb = (1 / (1 + Math.exp(-sumOfProb))); 
		
		System.out.println(totalProb);
		
		if ((totalProb) >= 0.5)
		{
			return true;
		}
		return false; 
	}
}
