1. Navigate to project folder so if testing on ix: user@ix-trusty:~ $ cd Perceptron_Avg/
2. Next run javac -d bin src/trz/uoregon/edu/PerceptronAvg.java
3. Now navigate to bin directory. cd bin/
4. Finally, Run: java trz.uoregon.edu.PerceptronAvg <training-set> <test-set> <model-file>
5. After the program prints: "Program Finished" the model should now be printed
out to file with accuracy at the top.

Note: I have tested this on ix so it should work on your machine.
For spam data set I received 93.60000000000001%. You can actually randomize 
example order on the convergence rate by adding "true" as an argument to 
argument list. So:
java trz.uoregon.edu.PerceptronAvg <training-set> <test-set> <model-file> true

Thank you.  