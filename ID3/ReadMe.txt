1. Navigate to project folder so if testing on ix: user@ix-trusty:~ $ cd ID3/
2. Next run javac -d bin src/trz/uoregon/edu/id3/*.java
3. Now navigate to bin directory. cd bin/
4. Finally, Run: java trz.uoregon.edu.id3.ID3 <training-set> <test-set> <model-file>
5. The model should now be printed out to file with accuracy at the top.

Note: I have tested this on ix so it should work on your machine.
For data set 2 I received 70.0% because one label was off from instructor's 
solution. I could not figure out why after 5 hours of debugging but data set 1
and mushroom set are completely correct. Thank you.  