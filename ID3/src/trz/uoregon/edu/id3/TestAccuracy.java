package trz.uoregon.edu.id3;

import java.util.List;

/**
 * Class that calculates accuracy of ID3 algorithm. 
 * 
 * @author  Troy Zuroske, University of Oregon
 */

public class TestAccuracy 
{
	public static double calculateAccuracy(List<Integer> dataHypothesis,
			List<dataLine> dataValidatorTest) 
	{
		if (dataHypothesis.size() != dataValidatorTest.size()) 
		{
			return 0.0;
		} 
		else 
		{
			double correct = 0.0; 
			double incorrect = 0.0; 
			double percent = 100.0; 
			double percentAccurate = 0.0; 
			for (int i = 0; i < dataHypothesis.size(); i++) 
			{
				if (dataHypothesis.get(i) == null)
				{
					incorrect++;
				} 
				else if (dataHypothesis.get(i) == dataValidatorTest.get(i).positiveOrNegative)  

				{
					correct++;
				} else 
				{
					incorrect++;
				}
			}
			percentAccurate = (correct * percent / (correct + incorrect));
			
			return percentAccurate; 
		}
	}
}
