package trz.uoregon.edu.id3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import trz.uoregon.edu.id3.ChiSquaredCalculation.ChiCalculation;

/**
 * Implementation of ID3 algorithm for CIS 572. 
 * 
 * Used pseudo code: https://www.cs.swarthmore.edu/~meeden/cs63/f05/id3.html
 *
 * @author  Troy Zuroske, University of Oregon
 */

public class ID3 
{
	static String trainingFile;
	static String testData;
	static String modelFile;
	static treeNode root; 
	static double accuracy; 
	static String labels[]; 
	
	/**
	 * Main class that initializes algorithm. 
	 * 
	 * @param args - 1. training file
	 * 				 2. test file
	 * 				 3. tree output file
	 */
	
    public static void main(String args[]) 
    {
    	if (args.length == 3)
    	{	
			trainingFile = args[0]; 
			testData = args[1];
			modelFile = args[2];
    	}
    	else
    	{
    		System.err.println("Argument error: must be in format "
    				+ "<training-set> <test-set> <model-file>");
    		System.exit(0); 
    	}
    	
    	List<Integer> dataHypothesis = new ArrayList<Integer>();
    	ArrayList<dataLine> arrayListTrainingData = new ArrayList<dataLine>();
		ArrayList<dataLine> arrayListTestData = new ArrayList<dataLine>();
		
		dataExtractor(trainingFile, testData, arrayListTrainingData, 
				arrayListTestData);
    	ID3 runID3 = new ID3(); 	
    	runID3.trainAlg(arrayListTrainingData, EntropyCalculation.entropy, 
    			ChiSquaredCalculation.pVal001);
    	
    	for (dataLine entries : arrayListTestData) 
    	{
			int predictedCategory = root.getLabel(entries);
			dataHypothesis.add(predictedCategory);
		}
		accuracy = TestAccuracy.calculateAccuracy(dataHypothesis, arrayListTestData); 
		System.out.println(accuracy);
		ID3.root.writeToFile(modelFile, accuracy); 
    	
    }
    
/**
 * Opens training file and test file and stores all data and class into 
 * according data structure.
 * 
 * @param sTrainingData - string of path to training data
 * @param sTestingData - string of path to testing data
 * @param trainingDataLine - the training data 
 * @param testDataLine - the testing data 
 */
    
    public static void dataExtractor(String sTrainingData, String sTestingData,
			List<dataLine> trainingDataLine, List<dataLine> testDataLine) 
	{
		int unID = 0;
		try {
				BufferedReader newFileBuffer = new BufferedReader(new FileReader(sTrainingData));
				String line;
				String label;
				label = newFileBuffer.readLine(); 
				labels = label.split(","); 
			
				while ((line = newFileBuffer.readLine()) != null) 
				{
					line = line.replace(",", "");
					dataLine ins = new dataLine(line, unID++, labels);
					trainingDataLine.add(ins);
				}
				newFileBuffer.close();
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
		try {
				BufferedReader newFileBuffer = new BufferedReader(new FileReader(sTestingData));
				String line;
				newFileBuffer.readLine(); 
				while ((line = newFileBuffer.readLine()) != null) 
				{
					line = line.replace(",", "");
					dataLine ins = new dataLine(line, unID++, labels);
					testDataLine.add(ins);
				}
				newFileBuffer.close();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
    
/**
 * Begin training the algorithm making calls to creating the tree.
 * calculating entropy, information gain, and chi squared test.
 * 
 * @param dataLine - the training data 
 * @param entropyCalc - the entropy calculation 
 * @param chiCalc - the chi squared test calculation
 */
    public void trainAlg(ArrayList<dataLine> dataLine, EntropyCalculation 
    		entropyCalc, ChiCalculation chiCalc) 
    {
    	treeNode root = new treeNode(null, dataLine);
    	InformationGainCalculation.calculateGain(root, entropyCalc, chiCalc, 0);
		ID3.root = root;
	} 
}

/**
 * Global class for printing dashes in file.
 *
 */
class Dashes
{
	public static HashMap<String, List<Integer>> dashCount = new HashMap<String, List<Integer>>();  
	public static int numberOfDashes = 0; 
}

/**
 * treeNode class that holds the tree data structure and data in nodes and also
 * contains function for printing tree to file.
 *
 */
class  treeNode 
{
	Boolean print = false; 
	List<dataLine> allDataFromLine;
	int resultLabel = -1;
	int totalAtts;
	int nodeTestVal;
	treeNode parentNode;
	treeNode childrenNodeArry[];

	treeNode(treeNode parentNode, List<dataLine> allDataFromLine)
	{
		this.allDataFromLine = allDataFromLine;
		totalAtts = allDataFromLine.get(0).arryVals.size();
		nodeTestVal = -1;
		
		this.parentNode = parentNode;
		childrenNodeArry = new treeNode[dataLine.uniqueVals];


		int count[] = {0,0};
		for (dataLine vals : this.allDataFromLine)
			count[vals.positiveOrNegative]++;
		
		resultLabel = count[0] > count[1] ? 0 : 1;
	}

	public int getLabel(dataLine values) 
	{
		if (nodeTestVal == -1) 
		{
			return resultLabel;
		} 
		else 
		{
			Iterator<?> iterator = values.arryVals.entrySet().iterator();
	        int n = 0;
	        while(iterator.hasNext())
	        {
	            Entry<?, ?> entry = (Entry<?, ?>) iterator.next();
	            if(n == nodeTestVal)
	            {
	            	if (entry.getValue() != null)
	    			{
	    				return childrenNodeArry[values.arryVals.get(entry.getKey())].getLabel(values);
	    			} else 
	    			{
	    				return -1;
	    			}
	            }
	            n++;
	        }
		}
		return resultLabel;
	}
	
	public void writeToFile (String outputFile, double accuracy)
	{
		try 
		{
			PrintWriter writer = new PrintWriter(new FileWriter(outputFile));
			writer.println("Accuracy: " + accuracy + "% with Chi-squared test");
			writeTree(writer);
			writer.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public void writeTree(PrintWriter writer)
	{
		int printClass = 0; 
		List<Integer> temp = new LinkedList<Integer>(); ; 
		if (parentNode == null)
		{
			//skip
		}
		else {
			for (int i = 0; i < dataLine.uniqueVals; i++) 
			{
				String label = (String) allDataFromLine.get(0).arryVals.keySet().toArray()[parentNode.nodeTestVal];
		
				if ((!(Dashes.dashCount.containsKey(label))) && parentNode.parentNode != null)
				{
					Dashes.numberOfDashes++; 
					temp.add(0, Dashes.numberOfDashes);
					temp.add(1, 0); 
					Dashes.dashCount.put(label, temp); 
				}
				
				if (this == parentNode.childrenNodeArry[i])
				{	
					if (parentNode.parentNode == null)
					{
						writer.print(label + " = " + i + " : "); 
						Dashes.dashCount.clear();
						Dashes.numberOfDashes = 0;
					}
					else
					{
						for (int j = 0; j < Dashes.dashCount.get(label).get(0); j++)
						{
							writer.print("| ");
						}
						printClass = i; 
						writer.print(label + " = " + i + " : "); 
						Dashes.dashCount.get(label).set(1, (Dashes.dashCount.get(label).get(1) + 1)); 
						Dashes.numberOfDashes = Dashes.dashCount.get(label).get(0);
						if (Dashes.dashCount.get(label).get(1) == 2)
						{
							Dashes.dashCount.remove(label); 
						}
					}
				} 
			}
		}
		if (nodeTestVal != -1) 
		{
			for (int i = 0; i < dataLine.uniqueVals; i++)
			{
				if (childrenNodeArry[i] != null)
				{
					writer.println();
					childrenNodeArry[i].writeTree(writer);
				}
			}
			Dashes.numberOfDashes--; 
			if (Dashes.numberOfDashes == 1)
			{
				Dashes.dashCount.clear();
				Dashes.numberOfDashes--; 
			}
		} 
		else 
		{
			writer.print(parentNode.childrenNodeArry[printClass].resultLabel);
		}
	}
}


/**
 * DataLine class that holds line of data from file assigning an ID to it, 
 * classifying as positive or negative for the class of data, then stores data
 * in array. 
 *
 */
class dataLine
{
	int uniqueId;
	int positiveOrNegative; 
	LinkedHashMap<String, Integer> arryVals = new LinkedHashMap<String, Integer>(); 
	static final int uniqueVals = 2; 

	public dataLine(String line, int id, String labels[]) 
	{
		this.uniqueId = id;
		
		char temp = line.charAt(line.length() - 1); 
		positiveOrNegative = Character.getNumericValue(temp); 
		char[] charfts = line.toCharArray();

		for (int i = 0; i < charfts.length - 1; i++) 
		{
			this.arryVals.put(labels[i], Character.getNumericValue(charfts[i])); 
		}
	}
}

