package trz.uoregon.edu.id3;

/**
 * Class that calculates entropy of given attribute. 
 * 
 * Used pseudo code: https://www.cs.swarthmore.edu/~meeden/cs63/f05/id3.html
 * 
 * @author  Troy Zuroske, University of Oregon
 */

public abstract class EntropyCalculation 
{
	public abstract double getEntropy(int examples, int target);
	
	public static EntropyCalculation entropy = new EntropyCalculation() 
	{
		public double getEntropy(int examples, int target) 
		{
			double pValTarget = target / ((double) examples + (double) target);

			double pValExamples = examples / ((double) examples + (double) target);
			
			double resultEnt = 0;
			if (examples > 0)
			{
				resultEnt += -pValExamples * Math.log(pValExamples);
			}
			if (target > 0)
			{
				resultEnt += -pValTarget * Math.log(pValTarget);
			}

			return resultEnt / Math.log(2);
		}
	};
}

