1. Navigate to project folder so if testing on ix: user@ix-trusty:~ $ cd NaiveBayes/
2. Next run javac -d bin src/trz/uoregon/edu/nb.java
3. Now navigate to bin directory. cd bin/
4. Finally, Run: java trz.uoregon.edu.nb <training-set> <test-set> <beta> <model-file>
5. After the program prints accuracy the model should now be printed out to file with 
bias at the top.

Thank you. 