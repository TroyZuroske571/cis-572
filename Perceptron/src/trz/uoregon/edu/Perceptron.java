package trz.uoregon.edu;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Implementation of Perceptron algorithm for CIS 572. 
 * 
 * Used pseudo code: http://ciml.info/dl/v0_9/ciml-v0_9-ch03.pdf (page 41)
 *
 * @author  Troy Zuroske, University of Oregon
 */

public class Perceptron 
{
	static int MAX_ITER = 100;
	static double LEARNING_RATE = 0.1;           
	static String trainingFile;
	static String testData;
	static String modelFile;
	static double accuracy; 
	static String labels[]; 
	static int attributes; 
	static boolean bRandomize = false; 
	static long seed = System.nanoTime();
	
	/**
	 * Main class that initializes algorithm and reads in files placing data 
	 * into array lists.
	 * 
	 * @param args - 1. training file
	 * 				 2. test file
	 * 				 3. tree output file
	 * 
	 * @return nothing
	 */
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{
		ArrayList<ArrayList<Double>> trainingDataSet = new ArrayList<ArrayList<Double>>(); 
		ArrayList<ArrayList<Double>> testDataSet = new ArrayList<ArrayList<Double>>(); 
		List <Object> weightBias = new ArrayList<Object>();
		ArrayList <Double> finalWeights = new ArrayList <Double>();
		double finalBias; 
		String sLine; 
		String line[];
		String label;
		String finalLabels[] = null; 
		
		if (args.length == 3)
    	{	
			trainingFile = args[0]; 
			testData = args[1];
			modelFile = args[2];
    	}
		else if (args.length == 4)
		{
			trainingFile = args[0]; 
			testData = args[1];
			modelFile = args[2];
			bRandomize = true; 
		}
    	else
    	{
    		System.err.println("Argument error: must be in format "
    				+ "<training-set> <test-set> <model-file>");
    		System.exit(0); 
    	}
		
		try {
				BufferedReader newFileBuffer = new BufferedReader(new FileReader(trainingFile));
				label = newFileBuffer.readLine(); 
				labels = label.split(","); 
				attributes = (labels.length - 1); 
				finalLabels = Arrays.copyOf(labels, (labels.length - 1)); 
				if (bRandomize == true)
				{
					Collections.shuffle(Arrays.asList(finalLabels), new Random(seed));
				}
				while ((sLine  = newFileBuffer.readLine()) != null) 
				{		
					line = sLine.split(",");
					ArrayList<Double> convert = new ArrayList<Double>();
					for (int i = 0; i < line.length; i++)
					{
					    try 
					    {
					        convert.add(Double.parseDouble(line[i]));
					    } catch (NumberFormatException nfe) {};
					}
					
					trainingDataSet.add(convert); 
				}
				
				newFileBuffer.close();
				
				newFileBuffer = new BufferedReader(new FileReader(testData));
				newFileBuffer.readLine(); 
				while ((sLine  = newFileBuffer.readLine()) != null) 
				{		
					line = sLine.split(",");
					ArrayList<Double> convertTest = new ArrayList<Double>();
					for (int i = 0; i < line.length; i++)
					{
					    try 
					    {
					    	convertTest.add(Double.parseDouble(line[i]));
					    } catch (NumberFormatException nfe) {};
					}
					
					testDataSet.add(convertTest); 
				}
				
				newFileBuffer.close();
			} catch (Exception e) 
			{
				e.printStackTrace();
			}
		
			if (bRandomize == true)
			{
				Collections.shuffle(trainingDataSet);
			}
			weightBias = perceptronAlg(trainingDataSet);
			finalWeights = (ArrayList<Double>) weightBias.get(0); 
			finalBias = (double) weightBias.get(1); 
			
			try 
			{
				PrintWriter writer = new PrintWriter(new FileWriter(modelFile));
				writer.println("Bias: " + finalBias + "%");
				for (int i = 0; i < (finalLabels.length); i++)
				{
					writer.printf("%-20s %20.16f%%\n", finalLabels[i], finalWeights.get(i)); 
				}
				
				accuracy = accuracyTest(finalWeights, finalBias, testDataSet);
				writer.println("Accuracy: " + accuracy +"%");

				writer.close();
			} catch (IOException e)
			{
				e.printStackTrace();
			}
			System.out.println("Program finished.");
		}
	
	/**
	 * Implementation of the training of the algorithm from training data.
	 * 
	 * @param args        - trainingDataSet - training data
	 * 
	 * @return weightBias - A list containing the final weights for each attribute
	 * 						and the overall bias
	 */
	@SuppressWarnings("unchecked")
	public static List<Object> perceptronAlg(ArrayList<ArrayList<Double>> trainingDataSet) 
	{
		int iterCount = 0;
		boolean bConverge = false; 
		Double bias = 0.0;
		ArrayList<Double> weights = new ArrayList<Double>(); 
		weights.addAll(trainingDataSet.get(0));   
		weights.remove(weights.size() - 1); 
		for (int i = 0; i < weights.size(); i++) 
		{
			weights.set(i, 0.0); 
		}
		ArrayList<Double> example = new ArrayList<Double>(); 
		ArrayList<Double> result = new ArrayList<Double>(); 
		double label; 
		double predict = 0; 
		double sign; 
		List <Object> weightBias = new ArrayList<Object>();
		List <Object> weightBiasUpdate = new ArrayList<Object>();
		
		while (iterCount != MAX_ITER || bConverge == true)
		{
			bConverge = true;
			for (int i = 0; i < trainingDataSet.size(); i++)
			{
				label = trainingDataSet.get(i).get(trainingDataSet.get(i).size() - 1); 
				
				//set label to -1 to make accuracy easier to find
				if (label == 0)
				{
					label = -1.0; 
				}
				example.addAll(trainingDataSet.get(i));
				example.remove(example.size() - 1); 
				
				for (int k = 0; k < example.size(); k++)
				{
					result.add(k, (example.get(k) * weights.get(k))); 
				}
				for(Double d : result)
					predict += d;
				
				predict = (predict + bias); 
				
				if (predict < 0)
				{
					sign = -1;
				}
				else if (predict == 0)
				{
					sign = 0;
				}
				else
				{
					sign = 1; 
				}
				
				if ((predict * label) <= 0)
				{
					weightBiasUpdate = update(weights, bias, example, label, sign);
					weights = (ArrayList<Double>) weightBiasUpdate.get(0);
					bias = (Double) weightBiasUpdate.get(1); 
					bConverge = false;
					example.clear();
					result.clear();
					predict = 0;
				}
				else
				{
					example.clear();
					result.clear();
					predict = 0;
				}
			}
		iterCount++; 
			
		}
		weightBias.add(weights);
		weightBias.add(bias); 
		return weightBias;
	}
	
	/**
	 * Implementation of the update function from pseudo code. (page 41, line7)  
	 * 
	 * @param args        - 1. weights  - the current weights of the attributes
	 * 					    2. bias     - the current bias
	 * 					    3. examples - the current line of the training data
	 * 						4. label	- the current label of the data line
	 * 					    5. sign	    - the current sign calculate from data
	 * 
	 * @return weightBias - A list containing the updated weights for each attribute
	 * 						and the updated bias
	 */
	public static List<Object> update (ArrayList<Double> weights, Double bias,
							 ArrayList<Double> examples, Double label, double sign)
	{
		ArrayList<Object> weightBias = new ArrayList<Object>();
		
		for (int i = 0; i < weights.size(); i++)
		{
			weights.set(i, (weights.get(i) + (Double) (LEARNING_RATE * (label - sign) * examples.get(i))));  
		}
		
		bias += LEARNING_RATE * (label - sign); 
		weightBias.add(weights);
		weightBias.add(bias); 
		return weightBias; 
	}
	
	/**
	 * Calculates the accuracy of the algorithm using pseudo code. (page 41) 
	 * 
	 * @param args      - 1. weights  - the final weights of the attributes
	 * 					  2. bias     - the final bias
	 * 					  3. testDataSet - the test data we are running our model aginst
	 * 
	 * @return accuracy - The final accuracy of the model. 
	 */
	public static double accuracyTest (ArrayList<Double> finalWeights, 
			double finalBias, ArrayList<ArrayList<Double>> testDataSet)
	{
		double accuracy = 0.0; 
		double counter = 0.0;
		double sign, label, predict = 0; 
		ArrayList<Double> example = new ArrayList<Double>(); 
		ArrayList<Double> result = new ArrayList<Double>(); 
		
		for (int i = 0; i < testDataSet.size(); i++)
		{
			label = testDataSet.get(i).get(testDataSet.get(i).size() - 1); 
			if (label == 0)
			{
				label = -1.0; 
			}
			example.addAll(testDataSet.get(i));
			example.remove(example.size() - 1); 
			
			for (int k = 0; k < example.size(); k++)
			{
				result.add(k, (finalWeights.get(k) * example.get(k))); 
			}
			for(Double d : result)
				predict += d;
			
			predict = (predict + finalBias); 
			
			if (predict < 0)
			{
				sign = -1;
			}
			else if (predict == 0)
			{
				sign = 0;
			}
			else
			{
				sign = 1; 
			}
		
			if ((sign * label) == 1)
			{
				counter += 1; 
			}
			example.clear();
			result.clear();
			predict = 0.0;
		}
		accuracy = ((counter / testDataSet.size()) * 100);
		return accuracy;
	}
	

}
